from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
blank_and_null = {'blank': True, 'null': True}


class Item(models.Model):
    title = models.CharField(verbose_name=_('title'), max_length=255)
    info = models.TextField(verbose_name=_('info'))
    image = models.ImageField(verbose_name=_('image'),
        upload_to='images',
        **blank_and_null)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')
