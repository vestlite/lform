from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='IndexView'),
    url(r'^cbv/form1/$', views.Form1View.as_view(), name='Form1View'),
    url(r'^fbv/form1/$', views.form1_view, name='form1_view'),
    url(r'^success/$', views.SuccessView.as_view(), name='SuccessView'),

    url(r'^item/(?P<pk>[^/]+)/$', views.ItemDetailView.as_view(),
        name='ItemDetailView'),

    url(r'^cbv/item/create/$', views.ItemCreateUpdateView.as_view(),
        name='ItemCreateView'),
    url(r'^cbv/item/update/(?P<pk>[^/]+)/$',
        views.ItemCreateUpdateView.as_view(),
        name='ItemUpdateView'),

    url(r'^ajax/cbv/item/create/$', views.ItemCreateUpdateViewAjax.as_view(),
        name='ItemCreateViewAjax'),
    url(r'^ajax/cbv/item/update/(?P<pk>[^/]+)/$',
        views.ItemCreateUpdateViewAjax.as_view(),
        name='ItemUpdateViewAjax'),

    url(r'^fbv/item/create/$', views.item_create_update_view,
        name='item_create_view'),
    url(r'^fbv/item/update/(?P<pk>[^/]+)/$', views.item_create_update_view,
        name='item_update_view'),
]
