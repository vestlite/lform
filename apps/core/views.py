from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy, reverse, resolve
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView, TemplateView, UpdateView, DetailView
from django import forms

from . import models as core_models

import logging

log = logging.getLogger('views')


class IndexView(TemplateView):
    template_name = 'apps/core/index_view.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['item'] = core_models.Item.objects.first()

        return context


class SuccessView(TemplateView):
    template_name = 'apps/core/success_view.html'


# unbounded form
class Form1View(FormView):
    template_name = 'apps/core/form1_view.html'
    success_url = reverse_lazy('core:SuccessView')

    def get_form_class(self):
        class _Form(forms.Form):
            first_name = forms.CharField(label=_('first name'), required=True)
            last_name = forms.DateField(label=_('last name'), required=False)
            choice = forms.ChoiceField(label=_('choice'),
                choices=((1, _('val1')), (2, _('val2'))))
            choice_models = forms.ModelChoiceField(
                queryset=core_models.Item.objects.all())
            image = forms.ImageField(label=_('image'), required=False)

        return _Form

    def form_valid(self, form):
        log.debug('form valid')

        return super(Form1View, self).form_valid(form)


class Form1(forms.Form):
    first_name = forms.CharField(label=_('first name'), required=True)
    last_name = forms.DateField(label=_('last name'), required=False)
    choice = forms.ChoiceField(label=_('choice'),
        choices=((1, _('val1')), (2, _('val2'))))
    choice_models = forms.ModelChoiceField(
        queryset=core_models.Item.objects.all())
    image = forms.ImageField(label=_('image'), required=False)


def form1_view(request, *args, **kwargs):
    form = Form1(data=request.POST or None, files=request.FILES or None)

    if form.is_valid():
        return redirect('core:SuccessView')

    return render(request, 'apps/core/form1_view.html', {'form': form})


# bounded forms cbv
class ItemDetailView(DetailView):
    model = core_models.Item
    template_name = 'apps/core/item_detail_view.html'


class ItemCreateUpdateView(UpdateView):
    model = core_models.Item
    template_name = 'apps/core/item_create_update_view.html'

    def get_success_url(self):
        return reverse('core:ItemDetailView', kwargs={'pk': self.object.pk})

    def get_object(self, queryset=None):
        return self.model.objects.filter(pk=self.kwargs.get('pk')).first()

    def get_form_class(self):
        self_model = self.model

        class _Form(forms.ModelForm):
            class Meta:
                model = self_model
                fields = '__all__'  # TODO: remove ! only for test!!!

        return _Form

    def get_context_data(self, **kwargs):
        context = super(ItemCreateUpdateView, self).get_context_data(**kwargs)
        context['curr_view'] = resolve(self.request.path)
        return context


# bounded forms fbv
def item_create_update_view(request, *args, **kwargs):
    class _Form(forms.ModelForm):
        class Meta:
            model = core_models.Item
            fields = '__all__'

    instance = core_models.Item.objects.filter(pk=kwargs.get('pk')).first()

    form = _Form(instance=instance, data=request.POST or None)
    if form.is_valid():
        instance = form.save()
        return redirect('core:ItemDetailView', pk=instance.pk)
    return render(request, 'apps/core/item_create_update_view.html', {
        'form': form
    })


# ajax
class ItemCreateUpdateViewAjax(ItemCreateUpdateView):
    template_name = 'apps/core/item_create_update_view_ajax.html'

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response({'success': True})
