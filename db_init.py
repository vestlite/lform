import importlib
import os
import shutil

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lform.settings")
django.setup()

from django.db import connection
from django.core.management import call_command
from django.conf import settings
from django_dynamic_fixture import G, F
from apps.core import models as core_models


def init(is_reset=True, is_dir_clean=True):
    # HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
    if is_reset:
        call_command('reset_db', interactive=0)

    if is_dir_clean:
        for dir2clean in ['cache', ]:
            dir2clean = os.path.join(settings.BASE_DIR, 'media', dir2clean)
            if os.path.exists(dir2clean):
                shutil.rmtree(dir2clean)

    # call_command('syncdb', interactive=0, verbosity=1, database='default')
    migrate_apps = []

    for app in settings.INSTALLED_APPS:
        if app.startswith('apps'):
            try:

                dir2clean = os.path.join(settings.BASE_DIR,
                    app.replace('.', '/'), 'migrations')

                if os.path.exists(dir2clean):
                    shutil.rmtree(dir2clean)

                os.mkdir(dir2clean)
                migrate_apps.append(app.split('.')[-1])

            except Exception as e:
                print('error', e)

    call_command('makemigrations', *migrate_apps)

    for app in migrate_apps:
        module = importlib.import_module('apps.{}.migrations'.format(app))
        importlib.reload(module)

    call_command('migrate')

    for i in range(10):
        G(core_models.Item, title='title {}'.format(i), info='info {}'.format(i))
    # admin = G(core_models.User,
    #     username='admin',
    #     email='admin@mm.ru',
    #     is_active=True,
    #     is_staff=True,
    #     is_superuser=True)
    #
    # admin.set_password('1')
    # admin.save()


if __name__ == '__main__':
    init()
